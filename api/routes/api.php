<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register','App\Http\Controllers\AuthController@register');
Route::post('/login','App\Http\Controllers\AuthController@login');

Route::group([
    'middleware' => 'auth:api',
], function () {
    Route::post('/logout','App\Http\Controllers\AuthController@logout');

    Route::post('/posts','App\Http\Controllers\PostController@create');
    Route::patch('/posts/{post}','App\Http\Controllers\PostController@edit');
    Route::delete('/posts/{post}','App\Http\Controllers\PostController@delete');

    Route::post('/posts/{post}/comments','App\Http\Controllers\CommentController@create');
    Route::patch('/posts/{post}/comments/{comment}','App\Http\Controllers\CommentController@edit');
    Route::delete('/posts/{post}/comments/{comment}','App\Http\Controllers\CommentController@delete');
});

Route::get('/posts','App\Http\Controllers\PostController@list');
Route::get('/posts/{post}','App\Http\Controllers\PostController@get');

Route::get('/posts/{post}/comments','App\Http\Controllers\CommentController@list');

