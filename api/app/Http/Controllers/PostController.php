<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\PostRepositoryInterface;

class PostController extends Controller
{
    private $postRepository;

    public function __construct(PostRepositoryInterface $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * Create new post
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required|string',
            'content' => 'required|string',
            'image' => 'nullable|string',
        ]);

        $validatedData['user_id'] = auth()->user()->id;
        $validatedData['slug'] = str_replace(' ', '-', $validatedData['title']);
        $post = $this->postRepository->create($validatedData);
        return response()->json([
            'data' => $post
        ]);
    }

    /**
     * Update Post
     *
     * @param  string  $post
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function edit(string $post, Request $request)
    {
        $postData = $this->postRepository->get($post);
        if (!$postData) {
            return response()->json([
                'message' => 'Post not found',
            ], 404);
        }
        $validatedData = $request->validate([
            'title' => 'required|string',
        ]);
        $validatedData['oldTitle'] = $post;
        $validatedData['title'] = $validatedData['title'];
        $validatedData['slug'] = str_replace(' ', '-', $validatedData['title']);

        $post = $this->postRepository->update($validatedData);
        return response()->json([
            'data' => $post
        ]);
    }

    /**
     * Remove post
     *
     * @param  string $post
     * @return \Illuminate\Http\Response
     */
    public function delete(string $post)
    {
        $post = $this->postRepository->delete($post);
        return response()->json([
            'status' => "record deleted successfully"
        ]);
    }

    /**
     * List all posts
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        $posts = $this->postRepository->list($request->page);
        return response()->json([
            'data' => $posts
        ]);
    }

    /**
     * Get specific post
     *
     * @param  string $post
     * @return \Illuminate\Http\Response
     */
    public function get(string $post)
    {
        $post = $this->postRepository->get($post);
        if (empty($post)) {
            return response()->json([
                'message' => 'No query results for model [App\\Post].',
            ], 404);
        }
        return response()->json([
            'data' => $post
        ]);
    }
}
