<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\PostRepositoryInterface;
use App\Repository\CommentRepositoryInterface;

class CommentController extends Controller
{
    private $postRepository;

    private $commentRepository;

    public function __construct(PostRepositoryInterface $postRepository, CommentRepositoryInterface $commentRepository)
    {
        $this->postRepository = $postRepository;
        $this->commentRepository = $commentRepository;
    }

    /**
     * Create new comment
     *
     * @param  string  $post
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(string $post, Request $request)
    {
        $validatedData = $request->validate([
            'body' => 'required|string'
        ]);

        $post = $this->postRepository->get($post);
        if (empty($post)) {
            return response()->json([
                'message' => 'No query results for model [App\\Post].',
            ], 404);
        }

        $validatedData['parent_id'] = null;
        $validatedData['creator_id'] = auth()->user()->id;
        $validatedData['commentable_type'] = 'App\\Post';
        $validatedData['commentable_id'] = $post->id;

        $comment = $this->commentRepository->create($validatedData);
        return response()->json([
            'data' => $comment
        ]);
    }

    /**
     * Update Comment
     *
     * @param  string  $post
     * @param  int  $comment
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function edit(string $post, int $comment, Request $request)
    {
        $post = $this->postRepository->get($post);
        if (empty($post)) {
            return response()->json([
                'message' => 'No query results for model [App\\Post].',
            ], 404);
        }

        $commentData = $this->commentRepository->get($comment);
        if (!$commentData) {
            return response()->json([
                'message' => 'Comment not found',
            ], 404);
        }
        $validatedData = $request->validate([
            'body' => 'required|string',
        ]);
        $validatedData['creator_id'] = auth()->user()->id;
        $validatedData['id'] = $comment;

        $comment = $this->commentRepository->update($validatedData);
        return response()->json([
            'data' => $comment
        ]);
    }

    /**
     * Remove comment
     *
     * @param  string  $post
     * @param  int  $comment
     * @return \Illuminate\Http\Response
     */
    public function delete(string $post, int $comment)
    {
        $post = $this->postRepository->get($post);
        if (empty($post)) {
            return response()->json([
                'message' => 'No query results for model [App\\Post].',
            ], 404);
        }

        $comment = $this->commentRepository->get($comment);
        if (!$comment) {
            return response()->json([
                'message' => 'Comment not found',
            ], 404);
        }

        $this->commentRepository->delete($comment);
        return response()->json([
            'status' => "record deleted successfully"
        ]);
    }

    /**
     * List all comments of a post
     *
     * @param  string  $post
     * @return \Illuminate\Http\Response
     */
    public function list(string $post)
    {
        $comments = $this->commentRepository->list($post);
        return response()->json([
            'data' => $comments
        ]);
    }
}
