<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Register Account to System
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|min:5',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:7|confirmed',
        ]);
        $validatedData['password'] = bcrypt($validatedData['password']);
        $validatedData['email_verified_at'] = date('Y-m-d H:i:s');

        $user = DB::transaction(function () use ($validatedData) {
            $user = User::create($validatedData);
            $accessToken = $user->createToken('authToken')->accessToken;
            return [
                'user' => $user,
                'accessToken' => $accessToken
            ];
        });

        return response()->json([
            'data' => $user['user']
        ], 200);
    }

    /**
     * Login Credentials
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $validatedData = $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:7',
        ]);

        if (!auth()->attempt($validatedData)) {
            return response()->json([
                'message' => 'Unauthorized',
            ], 401);
        }

        $tokenObject = auth()->user()->createToken('authToken');
        $accessToken = $tokenObject->accessToken;
        $expires_at = date('Y-m-d H:i:s', strtotime($tokenObject->token->expires_at));
        return response()->json([
            'token' => $accessToken,
            'token_type' => 'bearer',
            'expires_at' => $expires_at
        ]);
    }

    /**
     * Logout user
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        $user = Auth::user()->token();
        $user->revoke();
        return response()->json([
            'message' => 'Successfully logout'
        ]);
    }
}
