<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    public $table = 'comments';

    /**
     * The primary key of the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if there are timestamps.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Indicates if the ids are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'creator_id', 'parent_id', 'commentable_id', 'body', 'commentable_type'
    ];

    /**
     * The attributes that should be visible in the response
     *
     * @var array
     */
    protected $visible = [
        'id', 'creator_id', 'parent_id', 'commentable_id', 'body', 'commentable_type', 'created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->hasOne(\App\Models\User::class, 'id', 'creator_id');
    }

    public function post()
    {
        return $this->hasOne(\App\Models\Post::class, 'id', 'commentable_id');
    }
}
