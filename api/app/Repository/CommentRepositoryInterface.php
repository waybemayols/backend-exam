<?php

namespace App\Repository;

use Illuminate\Database\Eloquent\Model;

/**
* Interface CommentRepositoryInterface
* @package App\Repository
*/
interface CommentRepositoryInterface
{
    /**
     * Get comment by id
     *
     * @param int $id
     */
    public function get($id);

    /**
     * Get all comments of a post
     *
     * @param int $post_id
     */
    public function list($post_id);

    /**
     * Create new comment
     *
     * @param array $data
     */
    public function create($data);

    /**
     * Edit existing comment
     *
     * @param array $data
     */
    public function update($data);

    /**
     * Delete existing comment
     *
     * @param int $comment
     */
    public function delete($comment);
}
