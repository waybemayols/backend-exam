<?php

namespace App\Repository\Eloquent;

use App\Models\Post;
use App\Models\Comment;
use App\Repository\CommentRepositoryInterface;

class CommentRepository implements CommentRepositoryInterface
{
    /**
     * Get comment by id
     *
     * @param int $id
     * @return collection
     */
    public function get($id)
    {
        return Comment::where('id', $id)->first();
    }

    /**
     * Get all comments of a post
     *
     * @param int $post_id
     * @return collection
     */
    public function list($post_id)
    {
        $commentTableName = (new Comment())->getTable();
        $postTableName = (new Post())->getTable();
        $comments = Post::where('slug', $post_id)
                        ->join($commentTableName, $postTableName . '.id', '=', $commentTableName. '.commentable_id')
                        ->get();
        return $comments;
    }

    /**
     * Create new comment
     *
     * @param array $data
     * @return collection
     */
    public function create($data)
    {
        return Comment::create($data);
    }

    /**
     * Update existing comment
     *
     * @param array $data
     * @return collection
     */
    public function update($data)
    {
        $comment = Comment::where('id', $data['id'])->first();
        $comment->body = $data['body'];
        $comment->creator_id = $data['creator_id'];
        $comment->save();
        return $comment;
    }

    /**
     * Delete existing comment
     *
     * @param int $comment
     * @return collection
     */
    public function delete($comment)
    {
        return Comment::where('id', $comment)->delete();
    }
}
