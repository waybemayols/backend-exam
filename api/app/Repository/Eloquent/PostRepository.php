<?php

namespace App\Repository\Eloquent;

use App\Models\Post;
use App\Repository\PostRepositoryInterface;

class PostRepository implements PostRepositoryInterface
{
    /**
     * Get post by title
     *
     * @param string $post_title
     * @return collection
     */
    public function get($post_title)
    {
        return Post::where('slug', $post_title)->first();
    }

    /**
     * Get all posts
     *
     * @param string $page
     * @return collection
     */
    public function list($page = null)
    {
        $posts = new Post;
        if(!empty($page)){
            $posts = $posts->paginate($page);
        } else {
            $posts = $posts->get();
        }
        return $posts;
    }

    /**
     * Create new post
     *
     * @param array $data
     * @return collection
     */
    public function create($data)
    {
        return Post::create($data);
    }

    /**
     * Update existing post
     *
     * @param array $data
     * @return collection
     */
    public function update($data)
    {
        $post = Post::where('slug', $data['oldTitle'])->first();
        $post->title = $data['title'];
        $post->slug = str_replace(' ', '-', $data['title']);
        $post->save();
        return $post;
    }

    /**
     * Delete existing post
     *
     * @param string $post
     * @return collection
     */
    public function delete($post)
    {
        return Post::where('slug', $post)->delete();
    }
}
