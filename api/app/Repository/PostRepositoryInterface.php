<?php

namespace App\Repository;

use Illuminate\Database\Eloquent\Model;

/**
* Interface PostRepositoryInterface
* @package App\Repository
*/
interface PostRepositoryInterface
{
    /**
     * Get post by id
     *
     * @param string $post_title
     */
    public function get($post_title);

    /**
     * Get all posts
     *
     * @param string $page
     */
    public function list($page = null);

    /**
     * Create new post
     *
     * @param array $data
     */
    public function create($data);

    /**
     * Edit existing post
     *
     * @param array $data
     */
    public function update($data);

    /**
     * Delete existing post
     *
     * @param string $post
     */
    public function delete($post);
}
